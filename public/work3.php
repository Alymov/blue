<?php
if ($_POST['confirm'] == 'Отправить')
{
    if ($_POST['check'] == '')
    {
        print ('Обязательно ознакомтесь с условиями');
        exit();
    }

    foreach ($_POST as $key => $val)
    {
        if (empty($val))
        {
            print ('Поля со звёздочкой обязательны для заполнения');
            exit();
        }
    }
	
	if (strlen($_POST['name'])<2 || strlen($_POST['name'])>32 || !preg_match('/^[а-яА-Я ]+$/u', $_POST['name']))
    {
           print ('Не подходящее имя');
           exit();
    }
	
    extract($_POST);

    $user = 'u17815';
    $pass = '9028103';
    $db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass);

    $name = $_POST['name'];
    $email = $_POST['email'];
    $date = $_POST['date'];
    $sex = $_POST['sex'];
    $con = $_POST['con'];
	$select[] = $_POST['select'];
	$immortal = (int)$select[0];
	$wall = (int)$select[1];
	$levitation = (int)$select[2];
    $bio = $_POST['bio'];

    try
    {
        $stmt = $db->prepare("INSERT INTO application (name, email, date, sex, con, immortal, wall, levitation, bio) VALUES (:name, :email, :date, :sex, :con, :immortal, :wall, :levitation, :bio)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':sex', $sex);
        $stmt->bindParam(':con', $con);
        $stmt->bindParam(':immortal', $immortal);
		$stmt->bindParam(':wall', $wall);
		$stmt->bindParam(':levitation', $levitation);
        $stmt->bindParam(':bio', $bio);
        $stmt->execute();
        print ('Отправлено');
        exit();
    }
    catch(PDOException $e)
    {
        print ('Ошибка: ' . $e->getMessage());
        exit();
    }
}

header('Location: /blue');
?>
